import os
import glob
import preprocess
import sys
import h5py
import zarr
import numpy as np
import subprocess
from osgeo import gdal
import dask.array as da
from dask.array.image import imread
from dask import delayed
import dask.dataframe as dd
from multiprocessing import Pool
from multiprocessing import Process

data_path = '/Volumes/MyBookThunderboltDuo/Archived_projects/p05_gabon_machine_learning/a02_input/gabon/03_satellite_250m'
# data_path = 'D:/Data/Codes/pcprojects/a1610_learn2map/data/test01/input'
os.chdir(data_path)
# in_file = glob.glob('geo_*.tif')
in_file = glob.glob(os.path.join(data_path, '*.tif'))
print(in_file)
out_file = 'test'

# loaded = [preprocess.zarr_load_tif(i, '{0}_{1}'.format(out_file, os.path.splitext(i)[0])) for i in in_file]
# y_training = loaded.pop(0)
# df = preprocess.array_reshape(loaded, y_training, name='training', valid_min=0)
# df2 = preprocess.array_reshape(loaded, loaded[-1], name='pred', valid_min=0)
# df.compute()
# df2.compute()

# # making rasters in geotiff
data_path = '/Volumes/MyBookThunderboltDuo/Archived_projects/p05_gabon_machine_learning/a04_gabon'
os.chdir(data_path)
df = dd.read_csv('yhat2_2015_*.csv')
# df = dd.from_delayed(df)
df_valid = df.set_index('id')
df_valid.compute()

# in0 = gdal.Open(in_file[-1], gdal.GA_ReadOnly)
# ndim = (in0.RasterYSize, in0.RasterXSize)
# print(ndim)
# flat_shape = int(ndim[0] * ndim[1])
# z0 = np.full((flat_shape,), np.nan)
# z0[df_valid.index.astype(int)] = df_valid.y
# z0[z0 < 0] = 0
# driver = gdal.GetDriverByName('GTiff')
# ds = driver.CreateCopy('{}.tif'.format('yhat_2015'), in0)
# ds.GetRasterBand(1).WriteArray(np.array(z0.reshape(ndim)))
# ds = None

s = preprocess.y_to_geotiff(df_valid, in_file[0], name='yhat2_2015')
s.visualize()
s.compute()