import os
import glob
import preprocess
import sys
import h5py
import zarr
import numpy as np
import subprocess
from osgeo import gdal
import dask.array as da
from dask.array.image import imread
from dask import delayed
import dask.dataframe as dd
from multiprocessing import Pool
from multiprocessing import Process

data_path = '/Users/xuliang/Documents/Codes/pcprojects/a1610_learn2map/data/test02/input'
# data_path = 'D:/Data/Codes/pcprojects/a1610_learn2map/data/test01/input'
os.chdir(data_path)
in_file = glob.glob('*.tif')
print(in_file)
out_file = 'test'

loaded = [preprocess.zarr_load_tif(i, '{0}_{1}'.format(out_file, os.path.splitext(i)[0])) for i in in_file]
y_training = loaded.pop(0)
# y_training = loaded.pop(6)
# y_test = loaded.pop(6)
# y_mask = loaded.pop(6)
df = preprocess.array_reshape(loaded, y_training, valid_min=0)

# df = dd.read_csv('tmp_0.csv')
# df2 = df[['y']]
# s = preprocess.y_to_da(df2, loaded[-1])
# s.visualize()
df.compute()
