import os
import glob
import preprocess
import sys
import h5py
import zarr
import numpy as np
import subprocess
from osgeo import gdal
import dask.array as da
from dask.array.image import imread
from dask import delayed
import dask.dataframe as dd
from multiprocessing import Pool
from multiprocessing import Process

# data_path = 'D:\Codes\pcprojects\\a1801_nisar\data\\test02\input'
data_path = '/Users/xuliang/Codes/pcprojects/a1801_nisar/data/test02/input'
os.chdir(data_path)
in_file = glob.glob('*.tif')
print(in_file)
out_file = 'test'

# loaded = [preprocess.zarr_load_tif(i, '{0}_{1}'.format(out_file, os.path.splitext(i)[0])) for i in in_file]
# y_training = loaded.pop(-1)
# # y_mask = loaded.pop(-1)
# df = preprocess.array_reshape_mxn(loaded, y_training, m=2, n=2, name='training_5x5', valid_min=0)
df = preprocess.array_reshape_rolling(in_file[:2], in_file[-1], name='new2_5x5', m=2, n=2, valid_min=0, chunk_size=5000)

# df = dd.read_csv('tmp_0.csv')
# df2 = df[['y']]
# s = preprocess.y_to_da(df2, loaded[-1])
# s.visualize()
# df.compute()

