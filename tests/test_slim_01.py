import os
import tensorflow as tf
import dask.dataframe as dd
import matplotlib.pyplot as plt
from dask import delayed
from distributed.worker_client import get_worker
from dask_tensorflow import start_tensorflow
from dask.distributed import Client
import tensorflow.contrib.slim as slim
import numpy as np

data_path = '/Users/xuliang/Documents/Codes/pcprojects/a1610_learn2map/data/test02/input'
os.chdir(data_path)
df = dd.read_csv('tmp_0.csv')
df.head()
x0 = np.array(df.loc[0:40000, 'x0':'x7'])
y0 = np.array(df.loc[0:40000, 'y'])[:, None]
x1 = np.array(df.loc[40000:51378, 'x0':'x7'])
y1 = np.array(df.loc[40000:51378, 'y'])[:, None]
mean, std = x0.mean(), x0.std()
x0 = (x0 - mean) / std
x1 = (x1 - mean) / std

train_log_dir = './tmp_train/'
test_log_dir = './tmp_test/'


def my_neural_network(input, is_training=False):
    net = slim.fully_connected(input, 512, scope='layer1-512-fc')
    net = slim.dropout(net, is_training=is_training, scope='layer1-dropout')
    net = slim.fully_connected(input, 256, scope='layer2-256-fc')
    net = slim.dropout(net, is_training=is_training, scope='layer2-dropout')
    net = slim.fully_connected(input, 128, scope='layer3-128-fc')
    net = slim.dropout(net, is_training=is_training, scope='layer3-dropout')
    net = slim.fully_connected(input, 64, scope='layer4-64-fc')
    net = slim.dropout(net, is_training=is_training, scope='layer4-dropout')
    net = slim.fully_connected(net, 1, activation_fn=None, scope='output')
    net = slim.dropout(net, is_training=is_training, scope='output-dropout')
    return net

with tf.Graph().as_default():
    y_hat = my_neural_network(x0, is_training=True)
    loss = tf.losses.mean_squared_error(y0, y_hat)
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=.001)
    train_tensor = slim.learning.create_train_op(loss, optimizer)
    loss_summary = tf.summary.scalar('loss', loss)
    slim.learning.train(train_tensor, train_log_dir,
                        number_of_steps=100, save_summaries_secs=300, save_interval_secs=600)



with tf.Graph().as_default():
    y_hat2 = my_neural_network(x1, is_training=False)
    names_to_values, names_to_updates = slim.metrics.aggregate_metric_map({
        "mean_absolute_error": slim.metrics.streaming_mean_absolute_error(y_hat2, y1),
        "mean_squared_error": slim.metrics.streaming_mean_squared_error(y_hat2, y1),
        "concat": slim.metrics.streaming_concat(y_hat2)
    })
    cpt_path = tf.train.latest_checkpoint(train_log_dir)
    metric_values = slim.evaluation.evaluate_once(
        master='',
        checkpoint_path=cpt_path,
        logdir=train_log_dir,
        eval_op=list(names_to_updates.values()),
        final_op=list(names_to_values.values()))

    names_to_values = dict(zip(names_to_values.keys(), metric_values[0:2]))
    for name in names_to_values:
        print('%s: %f' % (name, names_to_values[name]))

    plt.scatter(y1, metric_values[2])

# num_batches = 1
# with tf.Session() as sess:
#     sess.run(tf.global_variables_initializer())
#     sess.run(tf.local_variables_initializer())
#     for batch_id in range(num_batches):
#         sess.run(names_to_updates.values())
#     metric_values = sess.run(names_to_values.values())
#     for metric, value in zip(names_to_values.keys(), metric_values):
#         print('Metric %s has value: %f' % (metric, value))
